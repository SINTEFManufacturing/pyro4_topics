# coding=utf-8

"""
Initialization module for the Pyro4.Topics package.
"""

__author__ = "Morten Lind"
__copyright__ = "Morten Lind 2014"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten@lind.dyndns.dk"
__status__ = "Development"

from .topic_server import TopicServer
from .topic import Topic

def get_topic_server(ns_host='0.0.0.0', ts_name='Pyro.TopicServer'):
    """Interface for acquiring a topic server interface."""
    import Pyro4
    ns = Pyro4.naming.locateNS(ns_host)
    return Pyro4.Proxy(ns.lookup(ts_name))
