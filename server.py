#!/usr/bin/python3
# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "Morten Lind 2014-2018"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten@lind.dyndns.dk"
__status__ = "Development"

import argparse
import atexit
import sys
import logging
logging.basicConfig(level='DEBUG')
log = logging.getLogger('TopicServer')

import Pyro4


#print('{}'.format(Pyro4.config.dump()))
#logging.debug(Pyro4.config.dump())

import pyro4_utils as pu
from pyro4_topics import TopicServer

logging.debug('Startup arguments: {}'.format(sys.argv))

arg_parser = argparse.ArgumentParser('Pyro4 Topics')
pu.add_arguments(arg_parser)
# Add feature arguments
arg_parser.add_argument('--clean_interval', type=int, default=10.0,
                        help=""" The time between purging of unused topics. If a topic is
                        unsubscribed to, and have not had a publish
                        event during the past 'clean_interval', it
                        will be purged. A negative value indicates no
                        automatic cleaning will be performed.""")
arg_parser.add_argument('--ns_topics', action='store_true',
                        help="""Switch for entering topic names in the name server.""")
arg_parser.add_argument('--use_standard_ns', action='store_true',
                        help=""" Switch for using the standard name server from Pyro4.naming rather
                        than the extended wrapper in pyro4_utils.""")
arg_parser.add_argument('--use_serpent', action='store_true',
                        help=""" Switch for using the standard serializer from Pyro4, 'serpent',
                        rather than 'pickle' which is default in
                        pyro4_utils.""")
arg_parser.add_argument('--block', action='store_true',
                        help=""" If set, blocks the program and unlocks on SIGINT or SIGTERM.""")

args, unknown_args = arg_parser.parse_known_args()

log.setLevel(args.log_level)

if args.use_serpent:
    log.info('Using the Pyro4 standard serializer "serpent", rather than "pickle", which is default in pyro4_utils.')
    Pyro4.config.SERIALIZERS_ACCEPTED = ['serpent']
    Pyro4.config.SERIALIZER = 'serpent'

if not args.use_standard_ns:
    log.info('Using extended name server wrapper from pyro4_utils.')
    # Prevent the establishment of a connection to the topic server
    args.use_topic_server = False
    pu.init(args)
    name_server = pu.name_server
else:
    log.info('Using the standard Pyro4.naming name server.')
    import Pyro4.naming
    name_server = Pyro4.naming.locateNS(host=args.name_server_host)
    daemon = Pyro4.Daemon(host=args.daemon_host)
log.info('Creating TopicServer with log_level "{}"'
         .format(args.log_level))
if args.ns_topics:
    topics_name_server = name_server
else:
    topics_name_server = None
topic_server = TopicServer(topics_name_server,
                           clean_interval=args.clean_interval,
                           log_level=args.log_level)
log.info('Registering topic server as "{}"'
         .format(args.topic_server_name))
if args.use_standard_ns:
    topic_server_uri = daemon.register(topic_server)
    name_server.register(args.topic_server_name, topic_server_uri)
else:
    name_server.register_object(args.topic_server_name, topic_server_uri)
    

def _release():
    """Release ressources and unregister name server name."""
    log.info('Releasing ressources.')
    name_server.remove(args.topic_server_name)
    topic_server.stop()

atexit.register(_release)

log.info('Running server')
topic_server.start()

if args.use_standard_ns:
    daemon.requestLoop()
elif args.block:
    import signal
    import threading
    _stop_cond = threading.Condition()

    def sig_handler(sig, stack_frame):
        log.info('Exiting with signal number "{}"'.format(sig))
        with _stop_cond:
            _stop_cond.notify_all()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)
    with _stop_cond:
        _stop_cond.wait()

    log.info('Stopping')
else:
    pass
