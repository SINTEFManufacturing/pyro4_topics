# coding=utf-8

"""
Very simple Topic server for Pyro4.
"""

__author__ = "Morten Lind"
__copyright__ = "Morten Lind 2014"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten@lind.dyndns.dk"
__status__ = "Development"

import logging
import threading
import time

import Pyro4

from .topic import Topic

@Pyro4.expose
class TopicServer(threading.Thread):
    
    def __init__(self, 
                 name_server=None, 
                 clean_interval=-1.0, 
                 log_level=logging.DEBUG):
        """If 'name_server' is given topics are registered in the name server
        as 'PyroTopic.<topic_name>'.
        """
        threading.Thread.__init__(self)
        self.daemon = True
        self.__stop = False
        self._topics = {}
        # The private ns attribute is used to signal that no name
        # server is used if None
        self._ns = name_server
        self._log = logging.getLogger('TopicServer')
        self._log.setLevel(log_level)
        self._clean_interval = clean_interval
        self._latest_clean_time = time.time()
        self._topics_lock = threading.RLock()

    def _new_topic(self, topic_name, auto_clean=True):
        """Create and register a new topic."""
        with self._topics_lock:
            new_topic = Topic(topic_name, auto_clean=auto_clean)
            new_uri = self._pyroDaemon.register(new_topic)
            self._topics[topic_name] = new_topic
            if not self._ns is None:
                self._ns.register('Pyro.Topic::{}'.format(topic_name), new_uri)
            return new_topic

    def get_topic(self, topic_name, auto_clean=True):
        """Get the topic named 'topic_name'. It is created locally if
        it does not exist. 'auto_clean' will affect a newly created
        topics only.
        """
        with self._topics_lock:
            if topic_name not in list(self._topics.keys()):
                # This logging line fails in Python3. Why?
                #self._log.info('Registering a new topic "{}"'.format(str(topic_name)))
                self._topics[topic_name] = self._new_topic(
                    topic_name, auto_clean=auto_clean)
        return self._topics[topic_name]

    @Pyro4.oneway
    def add_topic(self, topic_name, topic_prx):
        """Add 'topic_prx' as the (external) topic for the given 'topic_name'.
        """
        with self._topics_lock:
            if topic_name in self._topics:
                return False
            else:
                self._topics[topic_name] = topic_prx
    
    @Pyro4.oneway
    def subscribe(self, topic_name, subscriber):
        """Add the 'subscriber' to the topic 'topic_name'."""
        with self._topics_lock:
            self.get_topic(topic_name).subscribe(subscriber)

    @Pyro4.oneway
    def unsubscribe(self, topic_name, subscriber):
        """Add the 'subscriber' to the topic 'topic_name'."""
        with self._topics_lock:
            self.get_topic(topic_name).unsubscribe(subscriber)

    @Pyro4.oneway
    def publish(self, topic_name, data):
        """Publish 'data' on the topic named 'topic_name'."""
        with self._topics_lock:
            self.get_topic(topic_name).publish(data)

    def get_topic_names(self):
        return list(self._topics.keys())

    @property
    def topic_map(self):
        return dict(self._topics)

    @property
    def topic_names(self):
        return list(self._topics.keys())

    @property
    def topics(self):
        return list(self._topics.values())

    def get_clean_interval(self):
        return self._clean_interval
    @Pyro4.oneway
    def set_clean_interval(self, new_clean_interval):
        self._clean_interval = new_clean_interval
    clean_interval = property(get_clean_interval, set_clean_interval)

    def _clean(self):
        self._log.debug('Running periodic cleaning')
        with self._topics_lock:
            rm_topics = []
            now = time.time()
            for tn,t in self._topics.items():
                if (len(t.subscribers) == 0 
                    and t.latest_activity_time < 
                    now - self._clean_interval and t.auto_clean):
                    rm_topics.append(tn)
            for rmtn in rm_topics:
                self._log.debug('Auto cleaning topic "{}"'.format(rmtn))
                nstn = 'Pyro.Topic::{}'.format(rmtn)
                if not self._ns is None:
                    self._ns.remove(nstn)
                del self._topics[rmtn]
        self._latest_clean_time = time.time()
        
    def stop(self):
        self.__stop = True
        if not self._ns is None:
            for tn in self._topics:
                nstn = 'Pyro.Topic::{}'.format(tn)
                self._ns.remove(nstn)

    def get_log_level(self):
        return self._log.getEffectiveLevel()
    @Pyro4.oneway
    def set_log_level(self, log_level):
        self._log.setLevel(log_level)
    log_level = property(get_log_level, set_log_level)

    def run(self):
        while not self.__stop:
            if self._clean_interval < 0.0:
                time.sleep(1.0)
            else:
                next_clean_time = (self._latest_clean_time 
                                   + self._clean_interval)
                while not self.__stop and time.time() < next_clean_time:
                    next_clean_time = (self._latest_clean_time
                                       + self._clean_interval)
                    # Sleep until next clean time, but at most 1 second.
                    time.sleep(min(1.0, max(0.0, next_clean_time - time.time())))
                if self.__stop:
                    break
                self._clean()
