# coding=utf-8

"""
Simple Topic class for the Pyro4 Topic Server.
"""

__author__ = "Morten Lind"
__copyright__ = "Morten Lind 2014"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten@lind.dyndns.dk"
__status__ = "Development"

import logging
import threading
import time

import Pyro4

@Pyro4.expose
class Topic(object):
    def __init__(self, name, auto_clean=True, log_level=logging.INFO):
        self._subscribers = []
        self._name = name
        self._auto_clean = auto_clean
        self._log = logging.getLogger('Topic::{}'.format(name))
        self._log.level = log_level
        self._publish_cond = threading.Condition()
        self._latest_topic_data = None
        self._latest_activity_time = time.time()
        self._lock = threading.Lock()

    def get_log_level(self):
        return self._log.getEffectiveLevel()
    @Pyro4.oneway
    def set_log_level(self, log_level):
        self._log.setLevel(log_level)
    log_level = property(get_log_level, set_log_level)
        
    @Pyro4.oneway
    def subscribe(self, subscriber):
        """Subscribe 'subscriber' to this topic."""
        self._log.debug('Subscribing "{}"'.format(str(subscriber)))
        with self._lock:
            if not subscriber in self._subscribers:
                self._subscribers.append(subscriber)
            else:
                self._log.warn(
                    'The subscriber "{}" was already registered'
                    .format(str(subscriber)))

    @Pyro4.oneway
    def unsubscribe(self, subscriber, method_name=None):
        """Unsubscribe 'subscriber'."""
        self._log.debug('Unsubscribing "{}"'.format(str(subscriber)))
        with self._lock:
            if subscriber in self._subscribers:
                self._subscribers.remove(subscriber)
            else:
                self._log.warn(
                    'The subscriber "{}" was not registered'
                    .format(str(subscriber)))

    def wait_for_next_event(self):
        with self._publish_cond:
            self._publish_cond.wait()

    def get_latest_topic_data(self):
        return self._latest_topic_data
    latest_topic_data = property(get_latest_topic_data)

    def get_latest_activity_time(self):
        return self._latest_activity_time
    latest_activity_time = property(get_latest_activity_time)

    def get_subscribers(self):
        """Too invasive?"""
        return self._subscribers
    subscribers = property(get_subscribers)

    def get_auto_clean(self):
        return self._auto_clean
    def set_auto_clean(self, new_ac):
        self._auto_clean = bool(new_ac)
    auto_clean = property(get_auto_clean, set_auto_clean)

    @Pyro4.oneway
    def publish(self, topic_data):
        """Publish 'topic_data' on this topic."""
        with self._lock:
            self._latest_topic_data = topic_data
            self._latest_activity_time = time.time()
            with self._publish_cond:
                self._publish_cond.notify_all()
            remove_list = []
            self._log.debug('Publish event data: "{}"'.format(topic_data))
            self._log.debug(
                'Publishing to subscribers: {}'
                .format(self._subscribers))
            for sub in self._subscribers:
                try:
                    self._log.debug('Calling back in publish')
                    sub.topic_callback(self._name, topic_data)
                except:
                    self._log.exception(
                        'Exception in calling subscriber "{}". Removing.'
                        .format(sub))
                    remove_list.append(sub)
            for rsub in remove_list:
                self._subscribers.remove(rsub)

    @Pyro4.expose
    @property
    def name(self):
        return self._name
