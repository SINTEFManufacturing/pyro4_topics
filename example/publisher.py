# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "Morten Lind 2014-2018"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten@lind.dyndns.dk"
__status__ = "Development"

import time

import Pyro4

ns = Pyro4.locateNS()
ts = Pyro4.Proxy(ns.lookup('TopicServer'))
abc_topic = ts.get_topic('ABC')

abc_count = 0
while True:
    pub_data = 'abc_data no. {} @ {}'.format(abc_count, time.time())
    print('Publishing: "{}"'.format(pub_data))
    abc_topic.publish(pub_data)
    abc_count += 1
    time.sleep(1)
