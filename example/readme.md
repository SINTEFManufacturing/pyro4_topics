# Example use of Pyro4.TopicServer #

Start a standard Pyro4 name server

```
$ python3 -im Pyro4.naming
```

Start a topic server using the standard name server and standard serializer

```
$ python3 -im pyro4_topics.server --use_standard_ns --use_serpent
```

Start the example publisher

```
$ python3 -i publisher.py
```

Start the example subscriber

```
$ python3 -i subscriber.py
```

