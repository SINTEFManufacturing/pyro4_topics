# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "Morten Lind 2014-2018"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten@lind.dyndns.dk"
__status__ = "Development"

import atexit

import Pyro4

ns = Pyro4.locateNS()
ts = Pyro4.Proxy(ns.lookup('TopicServer'))
t = ts.get_topic('ABC')


@Pyro4.expose
class TopicSubscriber(object):
    @Pyro4.callback
    def topic_callback(self, topic, data):
        print('Received: "{}"'.format(data))


tsub = TopicSubscriber()
daemon = Pyro4.Daemon()
daemon.register(tsub)
t.subscribe(tsub)

atexit.register(t.unsubscribe,(tsub,))

daemon.requestLoop()
